import extract


class AnalyseWeather:
    def min_diff(self, extract_obj):
        weather_dict = extract_obj.extract('assets/weather.dat')
        min = int(weather_dict['MxT'][0]) - int(weather_dict['MnT'][0])
        day = weather_dict['Dy'][0]
        for max, min_var, day_var in zip(weather_dict['MxT'], weather_dict['MnT'], weather_dict['Dy']):
            if min > max-min_var:
                min = max-min_var
                day = day_var
        return day
