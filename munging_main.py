import analyse_football
import football
import weather
import extract
import analyse

class munging_main:
    def main(self):
        ext = extract.Extract()
        analyse_obj = analyse.AnalyseWeather()
        weather_obj = weather.Weather(ext, analyse_obj)
        day = weather_obj.min_temp_spread()
        print('day', day)

        ext = extract.Extract()
        analyse_obj = analyse_football.Analyse_football()
        football_obj = football.Football(ext, analyse_obj)
        day = football_obj.min_diff_goals()
        print(day)


if __name__ == "__main__":
    munging_main_obj = munging_main()
    munging_main_obj.main()