class Football:
    def __init__(self, extract, analyse):
        self.ext = extract
        self.analyse = analyse

    def min_diff_goals(self):
        day = self.analyse.min_diff_goals(self.ext)
        return day
