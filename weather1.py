
fr = open('assets/weather.dat', 'r')
weather = fr.readlines()
keys = weather[0].split()[:4]
weather_dict = dict.fromkeys(keys,[]*len(keys))
print(weather_dict)

temp = 0
for x in weather:
    if temp <= 1:
        temp += 1 
        continue
    list_of_columns = x.split()
    header_list = list(keys)
    for header_value in range(0,4):
        weather_dict[header_list[header_value]] = weather_dict[header_list[header_value]] + [list_of_columns[header_value]]

print(weather_dict)
        
