class Weather:
    def __init__(self, extract, analyse):
        self.ext = extract
        self.analyse = analyse

    def min_temp_spread(self):
        day = self.analyse.min_diff(self.ext)
        return day
