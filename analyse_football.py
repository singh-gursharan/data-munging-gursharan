class Analyse_football:
    def min_diff_goals(self, extract_obj):
        football_dict = extract_obj.extract_football('assets/football.dat')
        min = int(football_dict['F'][0]) - int(football_dict['A'][0])
        team = football_dict['Team'][0]
        for max, min_var, team_var in zip(football_dict['F'], football_dict['A'], football_dict['Team']):
            if min > max-min_var:
                min = max-min_var
                team = team_var
        return team