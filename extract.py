class Extract:
    def extract_football(self,fotball_path):
        fr = open('assets/football.dat', 'r')
        football = fr.readlines()
        keys = football[0].split()
        football_dict = dict.fromkeys(keys,[]*len(keys))
        temp = 0
        for x in football:
            if temp <= 1:
                temp += 1 
                continue
            list_of_columns = x.split()
            if len(list_of_columns) <= 2:
                continue
            header_list = list(keys)
            for header_value, key_num in zip(range(0, len(list_of_columns)), range(-1, len(header_list))):
                try:
                    if(header_value == 0):
                        continue
                    if(header_value == 1):
                        value = list_of_columns[header_value]
                    elif(header_value == 7):
                        value = int(list_of_columns[header_value+1])
                    else:
                        value = int(list_of_columns[header_value])
                except(ValueError):
                    new_value = ''
                    for alpha_num in list_of_columns[header_value+1]:
                        if(ord(alpha_num) >= 48 and ord(alpha_num) <= 57):
                            new_value += alpha_num
                    if len(new_value) == 0:
                        value = None
                    else:
                        value = int(new_value)
                football_dict[header_list[key_num]] = football_dict[header_list[key_num]] + [value]
        print(football_dict)
        return football_dict

    def extract(self, weather_path):
        fr = open('assets/weather.dat', 'r')
        weather = fr.readlines()
        keys = weather[0].split()[:4]
        weather_dict = dict.fromkeys(keys,[]*len(keys))
        print(weather_dict)
        temp = 0
        for x in weather:
            if temp <= 1:
                temp += 1 
                continue
            list_of_columns = x.split()
            header_list = list(keys)
            for header_value in range(0,4):
                try:
                    value = int(list_of_columns[header_value])
                except(ValueError):
                    new_value = ''
                    for alpha_num in list_of_columns[header_value]:
                        if(ord(alpha_num) >= 48 and ord(alpha_num) <= 57):
                            new_value += alpha_num
                    if len(new_value) == 0:
                        value = None
                    else:
                        value = int(new_value)
                weather_dict[header_list[header_value]] = weather_dict[header_list[header_value]] + [value]
        print(weather_dict)
        return weather_dict
